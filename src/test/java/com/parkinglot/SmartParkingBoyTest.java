package com.parkinglot;

import com.parkinglot.exceptions.NoAvailablePositionError;
import com.parkinglot.exceptions.UnrecognizedTicketError;
import com.parkinglot.parkingboy.SmartParkingBoy;
import com.parkinglot.parkingboy.SmartParkingBoy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SmartParkingBoyTest {

    @Test
    void should_park_car_to_parkingLot_with_more_position_when_park_given_parkingLots_having_positions_a_smart_parkingBoy_a_car() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.assignParkingLotToParkingBoy("原神停车场", new ParkingLot());
        smartParkingBoy.assignParkingLotToParkingBoy("铁穹停车场", new ParkingLot());
        //when
        ParkingTicket genShinImpactTicket = smartParkingBoy.park(new Car("我应该停在原神停车场"));
        ParkingTicket tieQingImpactTicket = smartParkingBoy.park(new Car("我应该停在铁穹停车场"));
        //then
        assertEquals("原神停车场", genShinImpactTicket.getParkingLotName());
        assertEquals("铁穹停车场", tieQingImpactTicket.getParkingLotName());
    }

    @Test
    void should_return_parking_ticket_when_park_given_parking_lot_and_smart_parking_boy_and_car() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.assignParkingLotToParkingBoy("这是一个停车场的名字", new ParkingLot());
        //when
        Car car = new Car("car01");
        ParkingTicket ticket = parkingBoy.park(car);
        //then
        assertEquals(car, ticket.getCar());
    }

    @Test
    void should_return_the_parked_car_when_fetch_given_parking_lot_and_smart_parking_boy_and_parked_car() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.assignParkingLotToParkingBoy("这是一个停车场的名字", new ParkingLot());
        //when
        Car car = new Car("car01");
        ParkingTicket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);
        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_the_right_ticket_when_fetch_given_parking_lot_with_2_parked_cars_and_smart_parking_boy_and_2_tickets() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.assignParkingLotToParkingBoy("这是一个停车场的名字", new ParkingLot());
        Car car1 = new Car("car10001");
        Car car2 = new Car("car10002");
        //when
        ParkingTicket ticket1 = parkingBoy.park(car1);

        ParkingTicket ticket2 = parkingBoy.park(car2);
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_UnrecognizedTicketError_when_fetch_given_parking_lot_and_smart_parking_boy_and_wrong_ticket() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        Car car1 = new Car("car1001");
        Car car2 = new Car("car1002");
        parkingBoy.assignParkingLotToParkingBoy("这是一个停车场的名字", new ParkingLot());
        //when
        parkingBoy.park(car1);
        ParkingTicket wrongTicket = new ParkingTicket(car2, new ParkingSeat(car2));
        //then
        assertThrows(UnrecognizedTicketError.class, () -> parkingBoy.fetch(wrongTicket));
    }

    @Test
    void should_throw_unrecognizedTicketError_when_fetch_given_parking_lot_and_smart_parking_boy_and_provided_used_ticket() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        Car car1 = new Car("car1001");
        parkingBoy.assignParkingLotToParkingBoy("这是一个停车场的名字", new ParkingLot());
        //when
        ParkingTicket ticket = parkingBoy.park(car1);
        Car firstFetchedCar = parkingBoy.fetch(ticket);
        //then
        assertNotNull(firstFetchedCar);
        assertThrows(UnrecognizedTicketError.class, () -> parkingBoy.fetch(ticket));
    }

    @Test
    void should_throw_NoAvailablePositionError_when_park_given_parking_lot_without_position_a_smart_boy_a_car() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.assignParkingLotToParkingBoy("这是一个停车场的名字", new ParkingLot());
        //when
        for (int i = 1; i < 10; i++) {
            String carId = "car" + i;
            parkingBoy.park(new Car(carId));
        }
        ParkingTicket tenthTicket = parkingBoy.park(new Car("car10"));
        //then
        assertNotNull(tenthTicket);
        assertThrows(NoAvailablePositionError.class, () -> parkingBoy.park(new Car("car11")));
    }

    @Test
    void should_get_the_right_car_when_fetch_given_smart_parking_boy_with_2_parkingLots_having_cars_and_2_tickets() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        String parkingLotName1 = "东方海外停车场-the first parkingLot";
        String parkingLotName2 = "思沃特停车场-the second parkingLot";
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName1, parkingLot1);
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName2, parkingLot2);
        // the first parking log is run out of position
        ParkingTicket ticketByParkingLot1 = null;
        for (int i = 0; i < 10; i++) {
            String carId = "思沃特停车场car" + i;
            if (i == 5) ticketByParkingLot1 = parkingBoy.park(new Car(carId));
        }
        ParkingTicket ticketByParkingLot2 = parkingBoy.park(new Car("东方海外停车场car1"));
        //when
        //then
        assertNotNull(ticketByParkingLot1);
        assertNotNull(ticketByParkingLot2);
        assertEquals(new Car("思沃特停车场car5"), parkingBoy.fetch(ticketByParkingLot1));
        assertEquals(new Car("东方海外停车场car1"), parkingBoy.fetch(ticketByParkingLot2));
    }

    @Test
    void should_throw_UnrecognizedParkingTicketError_when_fetch_given_smart_parking_boy_with_2_parkingLots_and_a_unrecognized_ticket() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        String parkingLotName1 = "东方海外停车场-the first parkingLot";
        String parkingLotName2 = "思沃特停车场-the second parkingLot";
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName1, parkingLot1);
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName2, parkingLot2);
        // the first parking log is run out of position
        for (int i = 0; i < 10; i++) {
            String carId = "合法车-思沃特停车场car" + i;
            parkingBoy.park(new Car(carId));
        }
        parkingBoy.park(new Car("合法车-东方海外停车场car1"));
        //when
        //then
        // parkingTicket上的车是你的，但是对应的Seat上面停放的车(包括null)不是你的，这种就是unrecognized ticket
        ParkingTicket illegalParkingTicket1 = new ParkingTicket(new Car("不合法车"), new ParkingSeat(new Car("合法车-思沃特停车场car1")), parkingLotName1);
        ParkingTicket illegalParkingTicket2 = new ParkingTicket(new Car("不合法车"), new ParkingSeat(new Car("合法车-东方海外停车场car1")), parkingLotName2);
        assertThrows(UnrecognizedTicketError.class, () -> parkingBoy.fetch(illegalParkingTicket1));
        assertThrows(UnrecognizedTicketError.class, () -> parkingBoy.fetch(illegalParkingTicket2));
    }

    @Test
    void should_throw_UnrecognizedParkingTicketError_when_fetch_given_smart_parking_boy_with_2_parkingLots_and_a_used_ticket() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        String parkingLotName1 = "东方海外停车场-the first parkingLot";
        String parkingLotName2 = "思沃特停车场-the second parkingLot";
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName1, parkingLot1);
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName2, parkingLot2);
        // the first parking log is run out of position
        ParkingTicket parkingTicket1 = null;
        for (int i = 0; i < 10; i++) {
            String carId = "合法车-思沃特停车场car" + i;
            parkingBoy.park(new Car(carId));
            if (i == 5) parkingTicket1 = parkingBoy.park(new Car(carId));
        }
        ParkingTicket parkingTicket2 = parkingBoy.park(new Car("合法车-东方海外停车场car1"));
        //when
        //then
        // parkingTicket 被使用两次fetch，第二次应该要报UnrecognizedParkingTicketError
        assertNotNull(parkingTicket1);
        parkingBoy.fetch(parkingTicket1);
        ParkingTicket finalParkingTicket = parkingTicket1;
        assertThrows(UnrecognizedTicketError.class, () -> parkingBoy.fetch(finalParkingTicket));

        parkingBoy.fetch(parkingTicket2);
        assertThrows(UnrecognizedTicketError.class, () -> parkingBoy.fetch(parkingTicket2));
    }

    @Test
    void should_throw_NoAvailablePosition_when_park_given_smart_parking_boy_with_2_parkingLots_without_position_and_a_car() {
        //given
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        String parkingLotName1 = "东方海外停车场-the first parkingLot";
        String parkingLotName2 = "思沃特停车场-the second parkingLot";
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName1, parkingLot1);
        parkingBoy.assignParkingLotToParkingBoy(parkingLotName2, parkingLot2);
        // the first parking log is run out of position
        for (int i = 0; i < 10; i++) {
            parkingBoy.park(new Car("合法车-思沃特停车场car" + i));
            parkingBoy.park(new Car("合法车-东方海外停车场car" + i));
        }
        //when
        //then
        assertThrows(NoAvailablePositionError.class, () -> parkingBoy.park(new Car("我这辆车加不进去")));
    }

}
