package com.parkinglot;

import com.parkinglot.exceptions.NoAvailablePositionError;
import com.parkinglot.exceptions.UnrecognizedTicketError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car("car10001");
        //when
        ParkingTicket ticket = parkingLot.park(car);
        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car("car10001");
        //when
        ParkingTicket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);
        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_the_right_car_when_fetch_given_parking_lot_parked_2_cars_and_provided_2_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("car10001");
        Car car2 = new Car("car10002");
        //when
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_unrecognizedTicketError_when_fetch_given_parking_lot_parked_cars_and_provided_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("car1001");
        Car car2 = new Car("car1002");
        //when
        parkingLot.park(car1);
        ParkingTicket wrongTicket = new ParkingTicket(car2, new ParkingSeat(car2));
//        Car fetchedCar = parkingLot.fetch(wrongTicket);
        //then
//        assertNull(fetchedCar);
        assertThrows(UnrecognizedTicketError.class, () -> {
            parkingLot.fetch(wrongTicket);
        });
    }

    @Test
    void should_throw_unrecognizedTicketError_when_fetch_given_parking_lot_parked_cars_and_provided_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car("car1001");
        //when
        ParkingTicket ticket = parkingLot.park(car1);
        Car firstFetchedCar = parkingLot.fetch(ticket);
//        Car secondFetchedCar = parkingLot.fetch(ticket);
        //then
        assertNotNull(firstFetchedCar);
//        assertNull(secondFetchedCar);
        assertThrows(UnrecognizedTicketError.class, () -> {
            parkingLot.fetch(ticket);
        });
    }

    @Test
    void should_throw_NoAvailablePositionError_when_park_given_parking_lot_already_parked_10_cars() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        //when
        for (int i = 1; i < 10; i++) {
            String carId = "car" + i;
            parkingLot.park(new Car(carId));
        }
        ParkingTicket tenthTicket = parkingLot.park(new Car("car10"));
//        ParkingTicket eleventhTicket = parkingLot.park(new Car("car11"));
        //then
        assertNotNull(tenthTicket);
//        assertNull(eleventhTicket);
        assertThrows(NoAvailablePositionError.class, () -> {
            parkingLot.park(new Car("car11"));
        });
    }

    @Test
    void should_throw_unrecognizedTicketError_when_fetch_given_an_unrecognized_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        //when
        parkingLot.park(new Car("car10"));
        Car car = new Car("this car was not assigned a ticket");
        ParkingTicket unrecognizedTicket = new ParkingTicket(car, new ParkingSeat(car));
        //then
        assertThrows(UnrecognizedTicketError.class, () -> {
            parkingLot.fetch(unrecognizedTicket);
        });
    }

}
