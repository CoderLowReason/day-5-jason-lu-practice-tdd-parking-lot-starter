package com.parkinglot;

import com.parkinglot.exceptions.NoAvailablePositionError;
import com.parkinglot.exceptions.UnrecognizedTicketError;

import java.util.*;

public class ParkingLot {
    private String parkingLotName;
    private final Map<String, ParkingSeat> carIdSeatMap;
    private final int capacity;
    private static final int DEFAULT_CAPACITY = 10;
    public ParkingTicket park(Car car) {
        if (!this.hasMorePosition()) throw new NoAvailablePositionError();
        ParkingSeat takenSeat = new ParkingSeat(car);
        this.carIdSeatMap.put(car.getCarId(), takenSeat);
        return new ParkingTicket(car, takenSeat);
    }

    public Car fetch(ParkingTicket ticket) {
        if (ticket == null) throw new UnrecognizedTicketError();
        Car ticketCar = ticket.getCar();
        ParkingSeat seat = this.carIdSeatMap.get(ticketCar.getCarId());
        if (seat == null) throw new UnrecognizedTicketError();
        Car seatOwnerCar = seat.getCurrentParkingCar();
        if (!ticketCar.equals(seatOwnerCar)) throw new UnrecognizedTicketError();
        this.carIdSeatMap.remove(ticketCar.getCarId());
        return ticketCar;
    }

    public int getSize() {
        return this.capacity;
    }

    public boolean hasMorePosition() {
        return this.capacity > this.carIdSeatMap.size();
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        carIdSeatMap = new HashMap<>(capacity);
    }

    public ParkingLot() {
        this.capacity = DEFAULT_CAPACITY;
        carIdSeatMap = new HashMap<>(DEFAULT_CAPACITY);
    }
    public String getParkingLotName() {
        return parkingLotName;
    }

    public void setParkingLotName(String parkingLotName) {
        this.parkingLotName = parkingLotName;
    }
}
