package com.parkinglot.parkingboy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exceptions.NoAvailablePositionError;

import java.util.HashMap;

public class StandardParkingBoy extends ParkingBoy{

    @Override
    public ParkingTicket park(Car car) {
        return super.park(car);
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        return super.fetch(ticket);
    }

    public void assignParkingLotToParkingBoy(String parkingLotName, ParkingLot parkingLot) {
        super.assignParkingLotToParkingBoy(parkingLotName, parkingLot);
    }

    @Override
    protected ParkingLot findTargetParkingLot() {
        return this.findAvailableParkingLotWithHigherPriority();
    }

    private ParkingLot findAvailableParkingLotWithHigherPriority() {
        HashMap<String, Integer> entry = parkingLotNameMapsArray.get(0);
        String parkingLotName = entry.keySet().iterator().next();
        int priority = entry.values().iterator().next();
        for (int i = 1; i < parkingLotNameMapsArray.size(); i++) {
            int currentPriority = parkingLotNameMapsArray.get(i).values().iterator().next();
            String currentParkingLotName = parkingLotNameMapsArray.get(i).keySet().iterator().next();
            ParkingLot currentParkingLot = managedParkingLotNameParkingLotMap.get(currentParkingLotName);
            // if priority higher , park first
            // if same priority, the one who execute assignParkingLotToParkingBoy later parks first
            if (currentPriority > priority && currentParkingLot.hasMorePosition()) parkingLotName = currentParkingLotName;
        }
        ParkingLot parkingLot = managedParkingLotNameParkingLotMap.get(parkingLotName);
        if (!parkingLot.hasMorePosition()) throw new NoAvailablePositionError();
        return parkingLot;
    }

    public void assignParkingLotToParkingBoy(String parkingLotName, ParkingLot parkingLot, int parkingLotPriority) {
        this.assignParkingLotToParkingBoy(parkingLotName, parkingLot);
        parkingLotNameMapsArray.add(new HashMap<>(){{put(parkingLotName, parkingLotPriority);}});
//        this.sortParkingLotNamesByPriority(parkingLotName, parkingLotPriority);
    }

//    private void sortParkingLotNamesByPriority(String parkingLotName, int parkingLotPriority) {
//        HashMap<String, Integer> parkingLotNamePriorityMap = new HashMap<>(){{put(parkingLotName, parkingLotPriority);}};
//        for (int i = 0; i < parkingLotNameMapsArray.size(); i++) {
//            Integer priority = parkingLotNameMapsArray.get(i).values().iterator().next();
//            if (parkingLotPriority > priority)  {
//                parkingLotNameMapsArray.add(i, parkingLotNamePriorityMap);
//                return;
//            }
//        }
//        parkingLotNameMapsArray.add(parkingLotNameMapsArray.size(), parkingLotNamePriorityMap);
//    }
}
