package com.parkinglot.parkingboy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exceptions.NoAvailablePositionError;

import java.util.HashMap;

public class SuperSmartParkingBoy extends SmartParkingBoy{

    @Override
    public ParkingTicket park(Car car) {
        return super.park(car);
    }

    public ParkingTicket park(Car car, String parkingLotName) {
        ParkingLot parkingLot = this.managedParkingLotNameParkingLotMap.get(parkingLotName);
        ParkingTicket parkingTicket = parkingLot.park(car);
        parkingTicket.setParkingLotName(parkingLot.getParkingLotName());
        return parkingTicket;
    }


    @Override
    public Car fetch(ParkingTicket ticket) {
        return super.fetch(ticket);
    }

    @Override
    protected ParkingLot findTargetParkingLot() {
        Integer index = this.findIndexOfParkingLotWithMostAvailablePositions();
        ParkingLot parkingLot = this.reduceParkingLotPositionsByIndexAndReturnTheParkingLot(index);
        if (!parkingLot.hasMorePosition()) throw new NoAvailablePositionError();
        return parkingLot;
    }

    @Override
    protected Integer findIndexOfParkingLotWithMostAvailablePositions() {
        HashMap<String, Integer> entry = parkingLotNameMapsArray.get(0);
        ParkingLot maxParkingLot = this.managedParkingLotNameParkingLotMap.get(entry.keySet().iterator().next());
        float maxPositionsRate = (float) entry.values().iterator().next() / maxParkingLot.getSize();
        int indexOfMaxPositions = 0;
        for (int i = 1; i < parkingLotNameMapsArray.size(); i++) {
            HashMap<String, Integer> currentEntry = parkingLotNameMapsArray.get(i);
            ParkingLot currentParkingLot = this.managedParkingLotNameParkingLotMap.get(currentEntry.keySet().iterator().next());
            float currentParkingLotPositionsRate = (float) currentEntry.values().iterator().next() / currentParkingLot.getSize();
            if (currentParkingLotPositionsRate > maxPositionsRate) {
                maxPositionsRate = currentParkingLotPositionsRate;
                indexOfMaxPositions = i;
            }
        }
        return indexOfMaxPositions;
    }

    public void assignParkingLotToParkingBoy(String parkingLotName, ParkingLot parkingLot) {
        super.assignParkingLotToParkingBoy(parkingLotName, parkingLot);
    }
}
