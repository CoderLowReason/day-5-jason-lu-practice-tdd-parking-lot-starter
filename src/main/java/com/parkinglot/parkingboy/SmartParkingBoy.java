package com.parkinglot.parkingboy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exceptions.NoAvailablePositionError;

import java.util.HashMap;

public class SmartParkingBoy extends ParkingBoy{

    public void assignParkingLotToParkingBoy(String parkingLotName, ParkingLot parkingLot) {
        super.assignParkingLotToParkingBoy(parkingLotName, parkingLot);
        this.parkingLotNameMapsArray.add(new HashMap<>(){{put(parkingLotName, parkingLot.getSize());}});
    }

    @Override
    public ParkingTicket park(Car car) {
        return super.park(car);
    }

    @Override
    protected ParkingLot findTargetParkingLot() {
        Integer index = this.findIndexOfParkingLotWithMostAvailablePositions();
        ParkingLot parkingLot = this.reduceParkingLotPositionsByIndexAndReturnTheParkingLot(index);
        if (!parkingLot.hasMorePosition()) throw new NoAvailablePositionError();
        return parkingLot;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        return super.fetch(ticket);
    }

    protected ParkingLot reduceParkingLotPositionsByIndexAndReturnTheParkingLot(Integer index) {
        HashMap<String, Integer> entry = this.parkingLotNameMapsArray.get(index);
        String parkingLotName = entry.keySet().iterator().next();
        entry.put(parkingLotName, Math.max(0, entry.values().iterator().next() - 1));
        return this.managedParkingLotNameParkingLotMap.get(parkingLotName);
    }

    protected Integer findIndexOfParkingLotWithMostAvailablePositions() {
        HashMap<String, Integer> entry = parkingLotNameMapsArray.get(0);
        Integer maxPositions = entry.values().iterator().next();
        int indexOfMaxPositions = 0;
        for (int i = 1; i < parkingLotNameMapsArray.size(); i++) {
            HashMap<String, Integer> currentEntry = parkingLotNameMapsArray.get(i);
            Integer currentParkingLotPositions = currentEntry.values().iterator().next();
            if (currentParkingLotPositions > maxPositions) {
                maxPositions = currentParkingLotPositions;
                indexOfMaxPositions = i;
            }
        }
        return indexOfMaxPositions;
    }
}
