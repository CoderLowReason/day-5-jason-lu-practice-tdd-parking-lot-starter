package com.parkinglot.parkingboy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exceptions.UnrecognizedTicketError;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class ParkingBoy {
    protected final HashMap<String, ParkingLot> managedParkingLotNameParkingLotMap = new HashMap<>();
    protected final ArrayList<HashMap<String, Integer>> parkingLotNameMapsArray = new ArrayList<>();

    protected abstract ParkingLot findTargetParkingLot();
    protected ParkingTicket park(Car car) {
        ParkingLot parkingLot = this.findTargetParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);
        parkingTicket.setParkingLotName(parkingLot.getParkingLotName());
        return parkingTicket;
    };
    protected Car fetch(ParkingTicket ticket) {
        String parkingLotName = ticket.getParkingLotName();
        ParkingLot parkingLot = managedParkingLotNameParkingLotMap.get(parkingLotName);
        if (parkingLot == null) throw new UnrecognizedTicketError();
        return parkingLot.fetch(ticket);
    };
    protected void assignParkingLotToParkingBoy(String parkingLotName, ParkingLot parkingLot) {
        if (parkingLotName == null || parkingLot == null) return;
        parkingLot.setParkingLotName(parkingLotName);
        managedParkingLotNameParkingLotMap.put(parkingLotName, parkingLot);
    };
}
