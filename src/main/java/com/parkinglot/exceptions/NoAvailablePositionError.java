package com.parkinglot.exceptions;

public class NoAvailablePositionError extends RuntimeException{
    public NoAvailablePositionError() {
        super("No available position");
    }
}
