package com.parkinglot.exceptions;

public class UnrecognizedTicketError extends RuntimeException{
    public UnrecognizedTicketError() {
        super("Unrecognized parking ticket");
    }
}
