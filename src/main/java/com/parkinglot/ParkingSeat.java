package com.parkinglot;

import java.util.Objects;

public class ParkingSeat {
    private Car currentParkingCar;

    public ParkingSeat(Car currentParkingCar) {
        this.currentParkingCar = currentParkingCar;
    }
    public Car getCurrentParkingCar() {
        return currentParkingCar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParkingSeat)) return false;
        ParkingSeat that = (ParkingSeat) o;
        return Objects.equals(getCurrentParkingCar(), that.getCurrentParkingCar());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCurrentParkingCar());
    }

    @Override
    public String toString() {
        return "ParkingSeat{" +
                "parkingCar=" + currentParkingCar +
                '}';
    }
}
