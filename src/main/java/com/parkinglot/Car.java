package com.parkinglot;

import java.util.Objects;

public class Car {
    private String carId;

    public Car(String carId) {
        this.carId = carId;
    }

    public Car() {
    }

    public String getCarId() {
        return carId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return Objects.equals(carId, car.carId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carId);
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId='" + carId + '\'' +
                '}';
    }
}
