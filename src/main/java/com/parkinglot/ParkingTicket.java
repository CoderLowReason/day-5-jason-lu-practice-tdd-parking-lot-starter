package com.parkinglot;

public class ParkingTicket {
    private String parkingLotName;
    private final Car car;
    private ParkingSeat seat;

    public ParkingTicket(Car car, ParkingSeat seat) {
        this.car = car;
        this.seat = seat;
    }

    public ParkingTicket(Car car, ParkingSeat seat, String parkingLotName) {
        this(car, seat);
        this.parkingLotName = parkingLotName;
    }

    public Car getCar() {
        return car;
    }

    public ParkingSeat getSeat() {
        return seat;
    }

    public String getParkingLotName() {
        return parkingLotName;
    }

    public void setParkingLotName(String parkingLotName) {
        this.parkingLotName = parkingLotName;
    }
}
